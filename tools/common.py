#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  common.py
#
#  Copyright 2015 Karl Lindén <karl.linden.887@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

# Common classes and functions for the scripts.

import argparse
import copy
import os

known_kinds = ['ans', 'ex', 'sol']

# Whether to include answers, exercies and/or solutions in the generated
# file. Will be extracted from arguments and then brought here, into the
# global scope.
inc_ans = False
inc_ex  = False
inc_sol = False

def include_answers(b):
    global inc_ans
    inc_ans = b

def include_exercises(b):
    global inc_ex
    inc_ex = b

def include_solutions(b):
    global inc_sol
    inc_sol = b

def error(err):
    print(os.path.basename(__file__) + ': error: ' + err)
    exit(1)

# This is the class that handles year ranges. Note that the order
# relation is quite special. Let a year range be identified by a
# closed interval then the following rules apply.
# [a,b] <  [c,d] <=> b <  c
# [a,b] <= [c,d] <=> b <= c
# [a,b] >  [c,d] <=> a >  d
# [a,b] >= [c,d] <=> a >= d
# [a,b] == [c,d] <=> a == c and b == d
# Thus one should be careful since
# A >= B does not imply A == B or A > B.
# Two YearRanges are disjoint if A < B or A > B.
# Two YearRanges A and B are said to be adjacent if A and B are disjoint
# and there exist no year range C such that
#  A < C < B  or  B < C < A.
# If A and B are not disjoint or if they are adjacent then the sum of
# A and B is defined as the union of A and B.
#
# Next follows a few propositions that will be needed to prove that the
# merging algorithm in the Years class works.
#
# Proposition. Transitivity.
# --------------------------
# If A < B and B < C, then A < C.
# -----
# Proof
# -----
# Let
#   A = [a,b],  B = [c,d],  C = [e,f]
# then
#   A < B <=> [a,b] < [c,d] <=> b < c
# and
#   B < C <=> [c,d] < [e,f] <=> d < e.
# Since c <= d it follows that
#   b < e,
# but this is the same as
#   A < C
# and we are done.
class YearRange:
    def __init__(self, first, second=None):
        # Make sure strings that denote ranges are handled correctly.
        if isinstance(first, str):
            if '-' in first:
                if second is not None:
                    raise ValueError
                try:
                    first, second = first.split('-')
                except:
                    raise ValueError
        if second is None:
            second = first
        self.begin = int(first)
        self.end   = int(second)

    def __repr__(self):
        if self.begin == self.end:
            return str(self.begin)
        else:
            return str(self.begin) + '-' + str(self.end)

    __str__ = __repr__

    def __contains__(self, x):
        if isinstance(x, YearRange):
            return self.begin <= x.begin <= x.end <= self.end
        else:
            return YearRange(x) in self

    def __eq__(self, other):
        if isinstance(other, YearRange):
            return self.begin == other.begin and self.end == other.end
        else:
            return YearRange(other) == self

    def __lt__(self, other):
        if isinstance(other, YearRange):
            return self.begin < other.end
        else:
            return self < YearRange(other)

    def __le__(self, other):
        if isinstance(other, YearRange):
            return self.begin <= other.end
        else:
            return self <= YearRange(other)

    def __gt__(self, other):
        if isinstance(other, YearRange):
            return self.begin > other.end
        else:
            return self > YearRange(other)

    def __ge__(self, other):
        if isinstance(other, YearRange):
            return self.begin >= other.end
        else:
            return self >= YearRange(other)

    def disjoint(self, other):
        return self < other or self > other

    def adjacent(self, other):
        return self.disjoint(other) and \
               (self.end + 1 == other.begin or \
                self.begin   == other.end + 1)

    def addable(self, other):
        return not self.disjoint(other) or self.adjacent(other)

    def __add__(self, other):
        if isinstance(other, YearRange):
            if self.addable(other):
                return YearRange(min(self.begin, other.begin),
                                 max(self.end, other.end))
            else:
                return ValueError
        else:
            return YearRange(other) + self

    __radd__ = __add__

# This is the class that abstracts operations on year ranges. It can
# take an initializer string that will be parsed in the creation of the
# class. Otherwise it will be initialized empty, and years can be added
# later on by simply adding to the class.
#
# Note that this class inherits a list. The ranges in this class are
# ordered so that
#   A_0, A_1, ..., A_n
# are not addable where the numbers denote the indices in the list. An
# empty list trivially fulfills this property so the to make this
# property always hold the _add_range method is written to preserve this
# property.
class Years(list):
    def __init__(self, init=None):
        list.__init__(self)
        if isinstance(init, str):
            if not ' ' in init:
                # It is just one range, so append it!
                self.append(YearRange(init))
            else:
                # Split the string into a list and handle it as any
                # other list.
                init = [p.strip(',') for p in init.split(' ')]
        if isinstance(init, list):
            for p in init:
                y = Years(p)
                self += y

    def __repr__(self):
        string = ''
        for r in self:
            if string:
                string += ', '
            string += repr(r)
        return string

    __str__ = __repr__

    # Adds years (ranges) to these years (ranges). This method merges
    # the ranges to be added into the ranges of this class and then
    # merges the ranges of this class to produce an ordered set where
    # no pair of ranges are addable.
    def _add_years(self, years):
        m = len(self)

        # If there are no ranges in the list the years can merely be
        # appended since the list is then trivially ordered and there
        # is no other range (that is addable with this one). In this
        # case we are done.
        if m == 0:
            self.extend(years)
            return

        # Let
        #   B_0, B_1, ..., B_{n-1}
        # be the ranges to be added and let the ones already in this
        # list be
        #   A_0, A_1, ..., A_{m-1}.
        n = len(years)

        # Definition
        # ----------
        # The ranges
        #   C_0, C_1, ..., C_k
        # are said to be independent if no two distinct C's are addable.

        # The general assumption is of course that the A's are
        # independent and all the B's are independent. Also both the A's
        # and the B's are ordered so that A_i < A_{i+1} and
        # B_j < B_{j+1}.

        # Proposition
        # -----------
        # If for some i < n and j < m, A_i and B_j are independent and
        # it holds that A_i < B_j, then
        #   A_0, A_1, ..., A_i, B_j, B_{j+1}, ..., B_n
        # are independent and
        #   A_0 < A_1 < ... < A_i < B_j < B_{j+1} < ... < B_n.
        # -----
        # Proof
        # -----
        # By assumption we have
        #   A_0 < A_1 < ... < A_i < B_j < B_{j+1} < ... < B_{n-1}
        # so it only remains to be proven that they are independent.
        # It is clear that all ranges are disjoint. By assumption A_i
        # and B_j are not addable. For any l > j B_j is a year range so
        # that
        #   A_i < B_j < B_l
        # which gives that for any l > j A_i and B_l are independent.
        # Similar holds for k < i since
        #   A_k < A_i < B_j
        # so for all k < i A_k and B_j are independent. If both k < i
        # and l > j then
        #   A_k < A_i < B_l
        # so A_k and B_l are independent. This completes the proof.

        # Proposition
        # -----------
        # If for some i < n and j < m, A_i and B_j are addable, but
        # A_{i-1} and B_j are not addable and A_{i-1} < B_j. Then
        # A_{i-1} and A_i + B_j are not addable.
        # -----
        # Proof
        # -----
        # The fact that A_{i-1} < B_j and the fact that they are not
        # addable gives that there exists a C such that
        #   A_{i-1} < C < B_j
        # Note that the same holds for A_{i-1} and A_i so we have
        #   A_{i-1} < D < A_i
        # for some D. Let
        #   A_i = [a,b],  B_j = [c,d],  C = [e,f],  D = [g,h]
        # Since A_i + B_j = [min(a,c),max(b,d)] it must hold that
        #   C < A_i + B_j  or  D < A_i + B_j,
        # and the proof is complete.

        # Start at the beginning of both lists.
        i = 0
        j = 0

        # We want to go through all B_j.
        while j < n:
            while i < m and \
                  not self[i].addable(years[j]) and \
                  years[j] > self[i]:
                i += 1

            # Now one of the below cases are possible. Note that the
            # while loop is a sequencing point (since the ands are
            # evaluated in order).
            # (A) i >= m.
            # (B) i < m and A_i and B_j are addable.
            # (C) i < m and A_i and B_j are not addable and B_j is not
            #     greater than A_i.
            # The cases are handled one by one.

            # In the case of (A) there were no ranges already in the
            # list that were addable and B_j was greater than all ranges
            # in the list. Hence, B_j is independent to every A_i.
            # The first proposition gives that
            #   A_0, A_1, ..., A_{n-1}, B_j, B_{j+1}, B_{m-1}
            # are independent and increasing. Thus, the list can be
            # extended with the rest of the B's and we are done.
            if i >= m:
                self.extend(years[j:])
                return

            # In the case of (B) there is an A_i that is addable with
            # B_j. Note that A_{i-1} is not addable with B_j and that
            # A_{i-1} < B_j. (Othewise otherwise the while loop would
            # have breaked at n-1). The second proposition gives that
            # A_{i-1} and A_i + B_j are not addable. Thus only
            #   A_{i+1}, A_{i+2}, ..., A_{m-1}
            # need to be checked for addability with the sum A_i + B_j.
            elif self[i].addable(years[j]):
                self[i] += years[j]

                # As long as A_{i+1} is addable it can be added to
                # the _new_ range A_i and then A_{i+1} can be removed
                # (and the number of A's must be adjusted.
                while i + 1 < m and self[i].addable(self[i+1]):
                    tmp = self.pop(i+1)
                    m -= 1
                    self[i] += tmp

                # Now either there are no more A's after A_i or there
                # are, which must be independent. Hence, all A's from
                # A_i an onwards are independent. It is also obvious
                # that (the new) A_i < A_{i+1} since the A's were
                # ordered. Hence, the A's now again fulfills the
                # assumptions that were required the top of this loop
                # so this process can be repeated for the next B.
                j += 1

            # In the last case (C), A_i and B_j are not addable and
            # B_j is not greater than A_i. Hence, it must be (by the
            # fact that B_j and A_i are not addable) that
            #   B_j < C < A_i  (*)
            # for some C since otherwise the inequalities would have to
            # be reversed, which is a contradiction since transitivity
            # of the inequalities then would give A_i < B_j. Thus
            # (*) must hold. Note that A_0 < A_1 < ... < A_{i-1} < B_j
            # and these must also be independent since otherwise the
            # while loop must have breaked earlier. Hence the first
            # proposition gives that
            #   A_0 < ... < A_{i-1} < B_j < A_i < ... < A_m
            # are independent. This means B_j can be insered at the
            # position of A_i while preserving the required properties.
            else:
                self.insert(i, years[j])
                m += 1

                # Now we can continue with the next B. Note that i < m
                # and B_{j+1} and B_j (the new A_i) are not addable and
                # B_{j+1} > B_j so the first run in the first nested
                # while loop will be unconditionally run. Thus,
                # increment i here.
                j += 1
                i += 1

    def __iadd__(self, other):
        # If the other operand is a string it will first be parsed into
        # a years structure and will then be added.
        if isinstance(other, str):
            other = Years(other)
        elif not isinstance(other, Years):
            return NotImplemented
        self._add_years(other)
        return self

    def __add__(self, other):
        cpy = copy.deepcopy()
        cpy += other
        return cpy

    __radd__ = __add__

# Takes a string on the form:
# YY_1, YY_2, YY_3-YY_4 First_name Surname <email> as initializer.
class Author:
    def __init__(self, string):
        fields = string.split(' ')

        # Extract the copyright year string.
        run = True
        yrs = []
        while run:
            y = fields[0]
            y = y.strip(',')
            for x in y:
                if not x.isnumeric() and x != '-':
                    run = False
            if run:
                yrs.append(y)
                fields[:1] = []
        if not yrs:
            error('Author string %s lacks copyright year' % string)
        self.years = Years(yrs)

        # Extract the email from the string and make sure it has the
        # correct form. Then strip < and > from it.
        self.email = fields.pop(-1)
        if not self.email.startswith('<') and \
           not self.email.endswith('>'):
            error('Invalid email address %s' % self.email)
        self.email = self.email[1:-1]

        # Extract the surname and complain if it does not exist.
        try:
            self.surname = fields.pop(-1)
        except IndexError:
            error('Invalid author string %s' % string)

        # Finally, extract the first name.
        self.first = ' '.join(fields)
        if not self.first:
            error('Author string %s lacks first name' % string)

        self.name = self.first + ' ' + self.surname

    def __eq__(self, other):
        if not isinstance(other, Author):
            return False
        return self.first == other.first and \
               self.surname == other.surname and \
               self.email == other.email and \
               self.years == other.years

    def __lt__(self, other):
        if self.surname < other.surname:
            return True
        elif self.surname == other.surname:
            return self.first < other.first
        else:
            return False

    def __le__(self, other):
        return self < other or self == other

    def __gt__(self, other):
        if self.surname > other.surname:
            return True
        elif self.surname == other.surname:
            return self.first > other.first
        else:
            return False

    def __ge__(self, other):
        return self > other or self == other

    def __repr__(self):
        return repr(self.years) + ' ' + \
               self.name + ' <' + self.email + '>'

    def __str__(self):
        return self.name

    def merge(self, other):
        if self.name != other.name:
            raise ValueError
        elif self.email != other.email:
            raise ValueError
        for y in other.years:
            self.years.add(y)

    def _replace_messy_characters(self, string):
        replacements = [
            ('_', '\\_'),
            ('å', '\\o{a}'),
            ('ä', '\\"{a}'),
            ('ö', '\\"{o}'),
            ('é', "\\'{e}")
        ]
        for (orig, repl) in replacements:
            string = string.replace(orig, repl)
        return string

    def latexname(self):
        return self._replace_messy_characters(self.name)

    # Returns a nicely formatted LaTeX string representing the author.
    def latex(self):
        string = self.first + \
                 ' \\textsc{' + self.surname + '} \\\\\n' + \
                 '<' + self.email + '> \\\\\n'
        return self._replace_messy_characters(string)

class Authors(dict):
    def __init__(self, cache=None):
        dict.__init__(self)
        if cache:
            f = open(cache, encoding='utf8')
            for line in f:
                line = line.strip('\n')
                self.add(Author(line))
            f.close()

    def __iadd__(self, other):
        if not isinstance(other, Authors):
            raise TypeError
        for x in other.values():
            self.add(x)
        return self

    def __add__(self, other):
        if not isinstance(other, Authors):
            raise TypeError
        s = Authors()
        s += self
        s += other
        return s

    def __repr__(self):
        string = ''
        for a in self.values():
            string += repr(a) + '\n'
        return string

    def __str__(self):
        string = ''
        for a in self.values():
            string += str(a) + '\n'
        return string

    def _sorted(self):
        return sorted(self.values())

    # Add an author to this class. It is a simple insertion if the
    # author does not already exist. Otherwise the authors are merged
    # if the email address does not differ.
    def add(self, auth):
        if not str(auth) in self.keys():
            self[str(auth)] = auth
        else:
            own = self[str(auth)]
            if not own == auth:
                if own.email != auth.email:
                    error('Author %s has differing email addresses %s '
                          'and %s' % (auth.name, auth.email, own.email))
                else:
                    # Only the years may differ. Merge them.
                    own.merge(auth)

    def latex(self):
        string = ''
        for a in self._sorted():
            if string:
                string += '\\and\n'
            string += a.latex()
        return string

    def latexnames(self):
        string = ''
        values = self._sorted()
        n = len(values)
        if n >= 2:
            for a in values[0:-2]:
                string += a.latexname() + ', '
            string += values[-2].latexname() + 'and '
        if values:
            string += values[-1].latexname()
        else:
            string = 'no author'
        return string

    def years(self):
        years = Years()
        for a in self._sorted():
            years += a.years
        return years

# This is basically a list with an authors function that distributes
# over its objects.
class AuthorableList(list):
    def append(self, obj):
        if not hasattr(obj, 'authors'):
            raise AttributeError
        list.append(self, obj)

    def authors(self):
        authors = Authors()
        for obj in self:
            authors += obj.authors()
        return authors

class Chapter:
    def __init__(self, c):
        self.subdir = c
        parts = self.subdir.split('-')
        if len(parts) != 2:
            error('Invalid chapter %s' % self.subdir)
        self.number = int(parts[0])
        if self.number <= 0:
            error('Invalid input number %s of input' % (self.number, i))
        if parts[1] != 'chap':
            error('Invalid chapter %s' % self.subdir)

    def authors(self, cache='authors.cache'):
        return Authors(os.path.join(self.subdir, cache))

class Input:
    def __init__(self, i):
        self.filename = os.path.basename(i)
        name, suffix = self.filename.split('.')
        parts = name.split('-')
        if len(parts) <= 1:
            error('Invalid input %s (too few fields)' % self.filename)
        self.number = int(parts[0])
        if self.number <= 0:
            error('Invalid input number %s of input' %
                  (self.number, self.filename))
        self.kind = parts[1]
        if not self.kind in known_kinds:
            error('Unknown kind %s of input %s' %
                  (self.kind, self.filename))
        if len(parts) >= 3:
            self.extra = parts[2]
            if len(parts) > 3:
                error('Too many fields in %s' % self.filename)
        else:
            self.extra = None

    def __eq__(self, other):
        if other == None:
            return False
        return self.number == other.number

    def authors(self):
        # The file header must look like this:
        # %
        # %  filename
        # %
        # %  Copyright YY_1, YY_2, YY_3-YY_4 First_name Surname <email>
        # %  Copyright YY_5 First_name_2 Surname_2 <email2>
        # %  ...
        # %  Copyright YY_m First_name_n Surname_n <emailn>
        # %
        # %  See the file COPYING-FDL-1.3 for copying conditions.
        # %
        f = open(self.filename, encoding='utf8')
        lines = [line.strip('\n') for line in f]
        f.close()
        if len(lines) < 7:
            error('%s: missing or short header' % self.filename)
        if lines[0] != '%' or \
           lines[1] != '%  ' + self.filename or \
           lines[2] != '%':
            error('%s: malformed header' % self.filename)
        lines[:3] = []

        # Now read copyright lines and parse the authors.
        authors = Authors()
        cstr = '%  Copyright ' # Copyright string to look for.
        l = lines.pop(0)
        if not l.startswith(cstr):
            error('%s: malformed header' % self.filename)
        l = l[len(cstr):]
        authors.add(Author(l))

        # There may be more copyright lines, but unlike above this is
        # not a requirement.
        while lines[0].startswith(cstr):
            l = lines.pop(0)
            l = l[len(cstr):]
            authors.add(Author(l))

        # The following lines should be fixed.
        expect = [
            '%',
            '%  See the file COPYING-FDL-1.3 for copying conditions.',
            '%',
            ''
        ]
        for e in expect:
            l = lines.pop(0)
            if l != e:
                error('%s: malformed header' & self.filename)

        return authors

class Exercise:
    def __init__(self, i):
        self.number    = i.number
        self.answers   = AuthorableList()
        self.exercises = AuthorableList()
        self.solutions = AuthorableList()
        self.append(i)

    def __nonzero__(self):
        return bool(self.answers) or \
               bool(self.exercises) or \
               bool(self.solutions)

    def __repr__(self):
        return str(self.number)

    __str__ = __repr__

    def __call__(self):
        string = ''
        if inc_ex:
            for e in self.exercises:
                string += '\\input{%s}\n' % e.filename
        if inc_sol:
            show_text   = False
            show_number = False
            if len(self.solutions) > 1:
                show_text = True
                show_number = True
            elif inc_ex:
                show_text = True
            i = 1
            for s in self.solutions:
                if show_text:
                    # Separate with an extra newline so that the text
                    # always ends up on a new line.
                    string += '\n'
                    if show_number:
                        string += '\\textit{Solution ' + str(i) + '.}\n'
                    else:
                        string += '\\textit{Solution.}\n'
                i += 1
                string += '\\input{%s}\n' % s.filename
        if inc_ans:
            show_text   = False
            show_number = False
            if len(self.answers) > 1:
                show_text = True
                show_number = True
            elif inc_ex or inc_sol:
                show_text = True
            i = 1
            for a in self.answers:
                if show_text:
                    # Separate with an extra newline so that the text
                    # always ends up on a new line.
                    string += '\n'
                    if show_number:
                        string += '\\textit{Answer ' + str(i) + '.}\n'
                    else:
                        string += '\\textit{Answer.}\n'
                i += 1
                string += '\\input{%s}\n' % a.filename
        return string

    def append(self, i):
        if i.kind == 'ans':
            self.answers.append(i)
        elif i.kind == 'ex':
            self.exercises.append(i)
        else: # i.kind == 'sol'
            self.solutions.append(i)

    def authors(self):
        authors = Authors()
        for i in self.answers + self.exercises + self.solutions:
            authors += i.authors()
        return authors

def add_common_arguments(parser):
    parser.add_argument('inputs', metavar='INPUT', type=str, nargs='*',
                        help='input files')
    parser.add_argument('--subdirs', '-d', type=str,
                        help='subdirs to include')
    parser.add_argument('--authors', '-u', type=str,
                        default='authors.cache',
                        help='authors cache file')

def inputs_to_exercises(inputs):
    exercises = AuthorableList()
    inputs.sort()

    e = None
    for f in inputs: # f for filename
        i = Input(f)
        if i == e:
            e.append(i)
        else:
            e = Exercise(i)
            exercises.append(e)

    return exercises

def common_data(args):
    exercises = inputs_to_exercises(args.inputs)

    if args.subdirs:
        subdirs = args.subdirs.split(',')
    else:
        subdirs = []
    subdirs.sort()
    chapters = AuthorableList()
    for s in subdirs:
        chapters.append(Chapter(s))

    return exercises, chapters, args.authors
