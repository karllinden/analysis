DESCRIPTION
-----------
Home page: https://gitlab.com/lilrc/analysis

This repository collects exercises, solutions and answers to the course
Analysis in one variable which is given by University of Lund, Sweden.

PREREQUISITES
-------------

Before you can build the sources to ready documents, you will need the
following programs.
 1. Git                [https://git-scm.com/]
 2. GNU Make           [https://www.gnu.org/software/make/]
 3. GNU Core Utilities [https://www.gnu.org/software/coreutils/coreutils.html]
 4. GNU Findutils      [https://www.gnu.org/software/findutils/]
 5. Python 3           [https://www.python.org/]
 6. LaTeX

If you run GNU/Linux, you most certainly already have 3,4,5 installed
and the packages for 1, 2 and 6 are usually called git, make and
texlive.
Consult the documentation for your package manager on how to install
those.

For users not running GNU/Linux, you will have to figure out how to
install the above.
You can always consult your favourite search engine.


HOW TO BUILD
------------

(Lines beginning with $ denotes commands that should be run.)

First you should download the source code.
This is simply done by cloning the git repository.
 $ git clone https://gitlab.com/lilrc/analysis.git
Then cd into the newly created directory.
 $ cd analysis
If you have properly installed the above PREREQUISITES you can simply
run
 $ make
and you should have four PDF files, exercises.pdf, answers.pdf,
solutions.pdf and full.pdf in the project directory.
If you want to speed up the building, you can add -jN to the command,
where N is the number of jobs to run in parallel.
For example:
 $ make -j4
If you also want to see the LaTeX generation output give make the
argument V=1, like this
 $ make V=1
Of course -jN and V=1 can be combined.


BUGS, MISTAKES AND QUESTIONS
----------------------------

Any issues should be posted on the gitlab project page. [1]
Just press the button that reads "+NEW ISSUE" and fill in the form.


CONTRIBUTING
------------

All contributors are welcome to improve this project! :)

To contribute it is of course necessary that you have git (see
PREREQUISITES) installed and know how to use it.
First fork the project at the project page.
Commit and push changes to your fork of the repository and then file
a merge request to the main repository.
You should consult some documentation on how to get going with git.

When contributing you must license your work under the GNU Free
Documentation License (if you are writing LaTeX sources).
This is done be including the correct header in your LaTeX source files.
You must also copyright your work to you.
Your name will then be mentioned as author in all documents that include
your work.
If you make changes to someone else's work, make sure to also add
yourself as copyright holder.
Also, add yourself to the AUTHORS file, if you haven't already.

The project is structured so that each chapter has its own subdirectory.
For example chapter 5 has the directory 05-chap.
All chapters should have directories on that form.
If the directory for a chapter you want to write material to does not
already exist, just create and the make program will find it.

Chapters can include exercises, solutions and/or answers.
Exercises, solutions and answers should be written in separate files.
For example the exercise description for exercise 5.18 is in the file
  05-chap/18-ex.tex.
Likewise holds for solutions
  05-chap/18-sol.tex
and answers
  05-chap/18-ans.tex.
These files will then be assembled together with other exercises to
produce the final output.
If there are many solutions (or answers or exercise descriptions) to
one exercise you can call your solution
  XY-sol-Z.tex
where Z is some identifier, preferably a number, where the most
preferred soltion has least number.

To maintain coherence and readability in the repository it is important
that the following rules are followed when making changes to the code.

 1. INDENTATION
    Always indent your LaTeX code.
    Each block should be indented by exactly two spaces.
    Blocks start at \begin{x} and ends at \end{x}.
    Also and \(, \[, \) and \] begin and end blocks, but in general you
    should not write
      \(
        x
      \)
    just for the sake of indenting.
    It should of course be \(x\) instead.
    If line breaking make things more readable, the broken line should
    indented.
    Like this, for example
    \[
      \int_a^b f(g(t))g'(t) = \int_{g(a)}^{g(b)} f(u) \d{u}
    \]
    1.1. EXCEPTION: DOCUMENT
         Although you will never have to write \begin{document} anywhere
         this is mentioned as an exception just for completeness, since
         the scripts do not indent the content of
         \begin{document} ... \end{document}.
 2. DOLLARS FORBIDDEN
    Never use dollars ($) or double dollars ($$).
    Use \( ... \) instead of $ ... $ and \[ ... \] instead of $$ ... $$.
    Double dollars are generally considered to be bad, but practically
    it does not matter if one uses $ or \(,\), but for consistency the
    latter should be used.
    See [2] and [3].
 3. LINE LENGTH
    Lines should be broken at 72 characters.
    Some exceptions are made to this rule, but not in the LaTeX sources.
    Hence, always break lines at 72 charactes and of course the lines
    should be line broken logically.
 4. TABLES
    Tables should be written using the table and tabular environments.
    The columns of the table should align in the LaTeX sources, which
    also means that rule 3 does not apply to tables.
 4. NO TWO SENTENCES ON THE SAME LINE
    Whenever starting a new sentence it should begin on a new line.
    This is both to ease the burden of rule 3, but also to ease working
    with the version control system (git, in this case).
 5. FILE HEADER
    Each file should begin with a header of a specified format.
    Look at the files already written, to see what this means.
 5. POSTSCRIPT
    All figures should be written in PostScript.

[1] https://gitlab.com/lilrc/analysis/issues
[2] https://stackoverflow.com/questions/2251417/latex-dollar-sign-vs
[3] ftp://ftp.dante.de/tex-archive/info/l2tabu/german/l2tabu.pdf
