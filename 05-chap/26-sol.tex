%
%  26-sol.tex
%
%  Copyright 2015 Karl Lindén <karl.linden.887@student.lu.se>
%
%  See the file COPYING-FDL-1.3 for copying conditions.
%

The proof is carried out by first proving a lemma that will be used
twice.

\begin{lemma}\label{lem:sol-05-26}
  If \(f\) is a continuous function on \([a,b]\) and \(P\) and \(P'\)
  are two paritions of \([a,b]\), such that \(P \subseteq P'\).
  Then
  \[ L(f,P) \le L(f,P') \le U(f,P') \le U(f,P). \]
\end{lemma}

The lemma can be proved using either the well-ordered principle or using
the equivalent induction principle.

\begin{proof}[Proof using the well-ordered principle]
  Let \(P = \{x_i, \; 0 \le i \le m\}\) and
  \(P' = \{x'_j, \; 0 \le j \le n\}\).
  Assume \(L(f,P') < L(f,P)\).
  Then
  \[
    L(f,P')
      = \sum_{j=1}^n f(l'_j) (x'_j - x'_{j-1})
      < \sum_{i=1}^m f(l_i) (x_i - x_{i-1})
      = L(f,P),
  \]
  where \(f(l_i)\) is the minimum on \([x_{i-1},x_i]\) and
  \(f(l'_j)\) is the minumum on \([x_{j-1},x_j]\).
  By the well-ordered principle there exists a least \(k\) such that
  \[
    \sum_{j=1}^q f(l'_j) (x'_j - x'_{j-1})
      < \sum_{i=1}^k f(l_i) (x_i - x_{i-1}),
  \]
  for some \(q\) such that \(x_k = x'_q\) and that
  \[
    \sum_{i=1}^{k-1} f(l_i) (x_i - x_{i-1})
      \le \sum_{j=1}^p f(l'_j) (x'_j - x'_{j-1}),
  \]
  for some \(x_{k-1} = x'_{p-1}\), but then,
  \[
    \begin{split}
      \sum_{j=1}^q f(l'_j) (x'_j - x'_{j-1})
        <
          \sum_{i=1}^k f(l_i) (x_i - x_{i-1})
        \\
        =
          \sum_{i=1}^{k-1} f(l_i) (x_i - x_{i-1}) +
            f(l_k) (x_k - x_{k-1})
        \\
        =
          \sum_{j=1}^{p-1} f(l'_j) (x'_j - x'_{j-1}) +
            f(l_k) (x_k - x_{k-1}),
    \end{split}
  \]
  from which it can be deduced that
  \[
    \sum_{j=p}^q f(l'_j) (x'_j - x'_{j-1}) < f(l_k) (x_k - x_{k-1}),
  \]
  where \(x'_{p-1} = x_{k-1}\) and \(x'_q = x_k\), but this means
  \[
    \begin{split}
      f(l_k) (x_k - x_{k-1})
        &= f(l_k) (x'_q - x'_{p-1})
        = f(l_k) \sum_{j=p}^q (x'_j - x'_{j-1})
        \\
        &= \sum_{j=p}^q f(l_k) (x'_j - x'_{j-1})
        > \sum_{j=p}^q f(l'_j) (x'_j - x'_{j-1})
    \end{split}
  \]
  Collecting all terms on one side yields
  \[
    \sum_{j=p}^q (f(l'_j) - f(l_k)) (x'_j - x'_{j-1}) < 0.
  \]
  Since the term \(x'_j - x_{j-1}\) is positive there must exist atleast
  one \(j\) such that
  \[ f(l'_j) - f(l_k) < 0 \iff f(l_k) > f(l'_j). \]
  Since \(l'_j \in [x_{k-1},x_k]\) this contradicts that \(f(l_k)\) is
  a minimum on the interval.
  Hence, the assumption that \(L(f,P') < L(f,P)\) is false and the
  desired result, \(L(f,P) \le L(f,P')\), is deduced.

  That \(U(f,P') \le U(f,P)\) can be proven similarly.
\end{proof}

\begin{proof}[Proof using induction]
  Let \(P = \{x_i, \; 0 \le i \le m\}\) and
  \(P' = \{x'_j, \; 0 \le j \le n\}\).
  Consider the \(i\):th term
  \[ f(l_i)(x_i - x_{i-1}) \]
  in the lower Riemann sum \(L(f,P)\).
  By the condition it is known that there exist \(x'_{p-1},x'_q \in P'\)
  such that
  \[
    x_{i-1} = x'_{p-1}
    \quad \text{and} \quad
    x_i = x'_q.
  \]
  % FIXME: is this necessary
  Above it must be that \(p \le q\), since if \(p-1 \ge q\), then by the
  fact that \((x'_j)_{j=0}^n\) is strictly increasing it would hold that
  \[ x_{i-1} = x'_{p-1} \ge x'_q = x_i \]
  which is a contradiction since also \((x_i)_{i=0}^m\) is strictly
  increasing.
  The above term can now be written as
  \[
    f(l_i) (x_i - x_{i-1})
      = f(l_i) (x'_q - x'_{p-1})
      = f(l_i) \sum_{j = p}^q (x'_j - x'_{j-1}).
  \]
  Since \(f(l_i)\) is the minimum value on \([x_{i-1},x_i]\), it must
  hold that
  \[ f(l_i) \le f(l'_j) \]
  for all \(j\) where \(f(l'_j)\) is the minimum on
  \([x'_{j-1},x'_j] \subseteq [x_{i-1},x_i]\).
  Hence,
  \begin{equation}\label{eq:sol-05-26-1}
    f(l_i) (x_i - x_{i-1}) \le \sum_{j = p}^q f(l'_j) (x'_j - x'_{j-1}),
  \end{equation}

  To complete the proof it shall be shown that
  \[
    L(f,P)
      =   \sum_{i=1}^m f(l_i) (x_i - x_{i-1})
      \le \sum_{j=1}^n f(l'_j) (x'_j - x'_{j-1})
      =   L(f,P').
  \]
  This can be done using induction, by showing
  \[
    \sum_{i=1}^k f(l_i) (x_i - x_{i-1})
    \le \sum_{j=1}^{q_k} f(l'_j) (x'_j - x'_{j-1})
  \]
  for all \(1 \le k \le m\) and some such that \(x_k = x'_{q_k}\).

  For \(k = 1\) there exist \(p\) and \(q_1\) such that
  \eqref{eq:sol-05-26-1} is fulfilled with \(i = 1\) and \(q = q_1\).
  Since \(P\) and \(P'\) are a partitions of \([a,b]\) it holds that
  \(x_0 = x'_0 = a\) and \(x'_0 < x'_1 < \cdots < x'_n\).
  Hence, as \(x'_{p-1} = x_0\), \(p\) must be \(1\).
  This completes the proof of the statement for \(k=1\), that is
  \[
    \sum_{i=1}^1 f(l_i) (x_i - x_{i-1})
    \le \sum_{j=1}^{q_1} f(l'_j) (x'_j - x'_{j-1}),
  \]
  where \(x_1 = x'_{q_1}\).

  Assume the statement has been proved for some \(k < m\).
  This means \(x_{k+1} \in P\) and by \eqref{eq:sol-05-26-1} there exist
  \(p\) and \(q_{k+1}\) such that \(x_k = x'_{p-1}\) and
  \(x_{k+1} = x_{q_{k+1}}\).
  Since \((x'_j)_{j=0}^n\) is strictly increasing, it must be that
  \(x'_{p-1} = x_k = x'_{q_k}\) by the induction hypothesis, from which
  it can be seen that \(p = q_k + 1\).
  This means
  \[
    f(l_i) (x_{k+1} - x_k)
      \le \sum_{j=q_k + 1}^{q_{k+1}} f(l'_j) (x'_j - x'_{j-1}),
  \]
  from which it can, by the induction hypothesis, is deduced that
  \[
    \sum_{i=1}^{k+1} f(l_i) (x_i - x_{i-1})
    \le \sum_{j=1}^{q_{k+1}} f(l'_j) (x'_j - x'_{j-1}),
  \]
  which is just the statement for \(k+1\) and this completes the
  induction step.

  If \(k = m\), the process stops and \(q_m = n\) since
  \(x'_{q_m} = x_m = b\) and the only \(x'\) fulfilling this is, by the
  definition of \(P'\), \(x'_n\).
  This completes the proof.
\end{proof}

Back to the exercise.
Let \(P'' = P \cup P'\).
Then \(P \subseteq P''\) and \(P' \subseteq P''\).
By using lemma \ref{lem:sol-05-26} twice it holds that
\[
  L(f,P) \le L(f,P'') \le U(f,P'') \le U(f,P'),
\]
which completes the proof. \qed
